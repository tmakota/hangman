module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-open');

    grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),
		typescript: {
		    base: {
		        src: [
		        	'ts/Game.ts'
		        	],
		        dest: 'build/game.js',
		        options: {
		            module: 'amd',
		            target: 'es5'
		        }
			} 
		},
        connect: {
            server: {
                options: {
                    port: 8080,
                    base: './'
                }
            }
        },
		watch: {
		    files: 'ts/*.ts',
		    tasks: ['typescript']
		},
		uglify: {
			build: {
				files: {
					'build/game.min.js': 'build/game.js'
				}
			}
		},
        open: {
            dev: {
                path: 'http://localhost:8080/index.html'
            } 
        }
	});

	grunt.registerTask('default', ['typescript', 'connect', 'open', 'watch']);
	grunt.registerTask('build', ['uglify']);
}