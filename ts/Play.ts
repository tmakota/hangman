/// <reference path="../source/phaser.d.ts"/>
/// <reference path="Constants.ts"/> 

module engine {
    export class Play extends Phaser.State {

        private background: Phaser.Sprite;
        private hangman: Phaser.Sprite;
        private congrat: Phaser.Sprite;
        
        private main: Phaser.Group;
        private bg: Phaser.Group;
        private hideLetter: Phaser.Group;
        private gallows: Phaser.Group;
        
        private play: Phaser.Button;
        
        private letters: Array<any>;
        private question: Array<any>;
        private currentLettersRound: Array<any>;
        private alphabet: Array<any>;
        private proccesWords: Array<Array<any>>;
        
        private maxValue: number;
        private score: number;
        private loseNum: number;
        
        private clickSound: Phaser.Sound;
        private winSound: Phaser.Sound;
        private loseSound: Phaser.Sound;
        private overSound: Phaser.Sound;

        private CLICK:string = 'Click!';
        
        private ROUND:string = 'Round:';
        private roundLabel: Phaser.Text;
        private roundCount: number;
        
        private whiteStyle:Object = { font: "16px Arial", fill: "#ffffff", align: "center" }

        create() {
            this.bg = this.add.group();
            this.main = this.add.group();
            this.background = this.add.sprite(0, 0, 'bg');
            this.bg.addChild(this.background);
            
            this.clickSound = this.add.audio('click');
            this.winSound = this.add.audio('win');
            this.loseSound = this.add.audio('lose');
            
            this.hideLetter = this.add.group();
            this.gallows = this.add.group();
            
            this.proccesWords = Constants.WORDS.slice();
            
            this.roundCount = 0;
            var round = `${this.ROUND}${this.roundCount}/${this.proccesWords.length}`;
            this.roundLabel = this.add.text(this.world.width - 20, 20, this.CLICK, this.whiteStyle);
            this.roundLabel.anchor.set(1, 0);
            
            this.loseNum = 0;
            this.currentLettersRound = [];
            this.alphabet = [];
            this.clearRound();
         
        }
        
        private resetQuestion():void{
            this.currentLettersRound = [];
            var w:number = (this.proccesWords.length > 1) ? Math.floor(Math.random()*this.proccesWords.length) : 0;
            this.question = this.proccesWords[w];
            this.proccesWords.splice(w, 1);
            this.maxValue = 0;
            
            this.question.map((l, index) => {
                var graphics:Phaser.Graphics = this.add.graphics((22 * index) + 15, 20);
                var text:Phaser.Text = this.add.text((22 * index) + 23, 40, l, this.whiteStyle);
                text.anchor.set(0.5);
                text.alpha = 0;
                this.currentLettersRound.push(text);
                graphics.alpha = 0.5;
                graphics.beginFill(0xffffff);
                graphics.drawRect(0, 0, 15, 30);
                graphics.endFill();
                
                graphics.name = l;
                this.hideLetter.addChild(graphics);   
            })
        }
        
        private cheackLetter(l:string):void{
            if(this.question.indexOf(l) >= 0){
                this.question.map((current, index) => {
                   if(l == current){
                       this.currentLettersRound[index].alpha = 1;
                       this.maxValue++;
                   }
                });
                if(this.maxValue == this.question.length)this.clearRound();
            }
            else this.updateHang();        
        }
        
        private clearRound():void {
            if(this.proccesWords.length == 0){
                this.gameOver(true);
            }
            else {
                this.roundCount++;
                var round = `${this.ROUND}${this.roundCount}/${Constants.WORDS.length}`;
                this.roundLabel.text = round;
                
                this.clearLetters();
                this.resetQuestion();
                this.resetButtons();
            }
        }
        
        private clearLetters():void {
            this.main.removeChildren(); 
            this.hideLetter.removeChildren();   
            this.currentLettersRound.map( t => t.destroy()); 
            this.alphabet.map( t => t.destroy());
        }
        
        private gameOver(res:boolean):void {
            this.clearLetters();
            this.roundLabel.destroy();
            if(res){
                this.congrat = this.add.sprite(this.world.width / 2, 10, 'congrat');
                this.gallows.addChild(this.congrat);
                this.congrat.anchor.set(0.5, 0);
                this.overSound = this.winSound;
                this.winSound.play();    
                
            }else {
                this.hangman = this.add.sprite(-10, 240, 'hangman');
                this.overSound = this.loseSound;
                this.loseSound.play();
                this.gallows.addChild(this.hangman);    
            }
            this.add.text(this.world.width / 2, 200, this.CLICK, this.whiteStyle);
            this.input.onDown.addOnce(this.gameMenu, this);
        }
        
        private gameMenu():void{
            this.overSound.stop();
            this.game.state.start('MainMenu', true, false);    
        }
        
        private updateHang():void {
            this.loseNum++;
            var part:Phaser.Sprite;
            switch (this.loseNum) {
                case 1:part = this.add.sprite(50, 220, 'v1');break;
                case 2:part = this.add.sprite(30, 243, 'v2');break;
                case 3:part = this.add.sprite(60, 55, 'v3');break;
                case 4:part = this.add.sprite(71, 50, 'v4');break;
                case 5:part = this.add.sprite(60, 31, 'v5');break;
                case 6:part = this.add.sprite(145, 45, 'v6');break;
                case 7:part = this.add.sprite(135, 45, 'man');break;
            }
            this.gallows.addChild(part);  
            if(this.loseNum == 7)this.gameOver(false);
        }
        
        private resetButtons():void{
            this.alphabet = [];
            this.letters = Constants.ALPHABET.slice();
            var y:number = 0;  
            var x:number = 0;
            var style:Object = { font: "16px Arial", fill: "#000000", align: "center" };
            
            this.letters.map((l) => {
                var graphics:Phaser.Graphics = this.add.graphics((22 * x) + 15, 350 + (40 * y));
                var text:Phaser.Text = this.add.text((22 * x) + 23, 370 + (40 * y), l, style);
                text.anchor.set(0.5)
                graphics.beginFill(0xffffff);
                graphics.drawRect(0, 0, 15, 30);
                graphics.endFill();
                graphics.name = l;
                this.main.addChild(graphics);
                this.alphabet.push(text);
                graphics.inputEnabled = true;
                graphics.events.onInputDown.add(function () {
                    this.clickSound.play();
                    var index:number = this.letters.indexOf(graphics.name);
                    this.letters.splice(index, 1);
                    this.main.remove(graphics);
                    text.destroy();
                    this.cheackLetter(graphics.name);
                }, this);
                
                x++;
                if(x > 12){x = 0; y++;}
            });
            
        }
        
        
    }
}