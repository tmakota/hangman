/// <reference path="../source/phaser.d.ts"/>
/// <reference path="Boot.ts"/>
/// <reference path="Preloader.ts"/>
/// <reference path="MainMenu.ts"/>
/// <reference path="Play.ts"/>

module engine {

    export class Game extends Phaser.Game {

        constructor() {

            super(310, 450, Phaser.AUTO, 'content', null);

            this.state.add('Boot', Boot, false);
            this.state.add('Preloader', Preloader, false);
            this.state.add('MainMenu', MainMenu, false);
            this.state.add('Play', Play, false);

            this.state.start('Boot');

        }

    }

} 