/// <reference path="../source/phaser.d.ts"/>
module engine {
    export class MainMenu extends Phaser.State {

        background: Phaser.Sprite;
        hangman: Phaser.Sprite;
        group: Phaser.Group;
        title: Phaser.Sprite;
        play: Phaser.Button;

        create() {
            this.group = this.add.group();
            this.group.alpha = 0;
            this.background = this.add.sprite(0, 0, 'bg');
            this.group.addChild(this.background);

            this.hangman = this.add.sprite(-20, 260, 'hangman');
            this.group.addChild(this.hangman);

            var tween = this.add.tween(this.group).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
            tween.onComplete.add(this.showTitle, this);
        }
        
        showTitle(){
            var rules = this.add.sprite(this.world.width, 140, 'rules');
            rules.anchor.setTo(1, 0); 
            this.group.addChild(rules);
            
            this.title = this.add.sprite(0, 0, 'title');   
            this.play = this.add.button(this.world.width, this.world.height, 'play', this.startGame, this);
            this.group.addChild(this.play);
            this.play.anchor.setTo(1); 
        }

        startGame() {
            this.game.state.start('Play', true, false);
        }
    }
}