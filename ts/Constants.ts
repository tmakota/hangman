module engine {
	export class Constants {
        public static ALPHABET:Array<string> = [
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
            ];
        public static WORDS:Array<Array<any>> = [
            ['O','N','E'],
            ['T','W','O'],
            ['T','H','R','E','E'],
            ['T','W','E','N','T','Y'],
            ['M','A','D','M','A','N']
        ]
	}
}