/// <reference path="../source/phaser.d.ts"/>
module engine {
    export class Boot extends Phaser.State {
        preload() {
            this.load.image('preloadBar' , './materials/images/loader.png');
        }

        create() {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;

            if (this.game.device.desktop) {
                this.scale.pageAlignHorizontally = true;
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            } else {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.minWidth = 310;
                this.scale.minHeight = 450;
                this.scale.maxWidth = 768;
                this.scale.maxHeight = 1024;
                this.scale.forceLandscape = true;
                this.scale.pageAlignHorizontally = true;
                this.scale.refresh();
            }

            this.game.state.start('Preloader', true, false);
        }
    }
}