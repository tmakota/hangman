/// <reference path="../source/phaser.d.ts"/>
module engine {
    export class Preloader extends Phaser.State {

        preloadBar: Phaser.Sprite;

        preload() {
            this.preloadBar = this.game.add.sprite(50, 250, 'preloadBar');
            this.load.setPreloadSprite(this.preloadBar);

            this.load.image('bg', './materials/images/bg.jpg');
            this.load.image('hangman', './materials/images/hangman.png');
            this.load.image('title', './materials/images/title2.png');
            this.load.image('play', './materials/images/PlayButton.png');
            this.load.image('rules', './materials/images/rules.png');
            this.load.image('congrat', './materials/images/win.png');
            
            //gallows
            this.load.image('v1', './materials/images/V1.png');
            this.load.image('v2', './materials/images/V2.png');
            this.load.image('v3', './materials/images/V3.png');
            this.load.image('v4', './materials/images/V4.png');
            this.load.image('v5', './materials/images/V5.png');
            this.load.image('v6', './materials/images/V6.png');
            this.load.image('man', './materials/images/man.png');
            
            //audio
            this.load.audio('click', './materials/sounds/click.mp3', true);
            this.load.audio('lose', './materials/sounds/lose.mp3', true);
            this.load.audio('win', './materials/sounds/win.mp3', true);
        }

        create() {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000,
                Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
        }

        startMainMenu() {
            this.game.state.start('MainMenu', true, false);
        }
    }
}